const defineKindOfTriangle = (sideA, sideB, sideC) => {
  const squaredSideA = Math.pow(sideA, 2);
  const squaredSideB = Math.pow(sideB, 2);
  const squaredSideC = Math.pow(sideC, 2);
  const squaredSidesAB = squaredSideA + squaredSideB;

  if (sideA >= sideB + sideC || sideB >= sideC + sideA || sideC >= sideA + sideB) {
    return 'not exist';
  }

  return (squaredSideC > squaredSidesAB) && 'obtuse' ||
    (squaredSideC < squaredSidesAB) && 'acute-angled' ||
    'rectangular';

};

module.exports = defineKindOfTriangle;
